## Seguridad Informática

Proyecto final de curso 2018-19 ASIX@EDT de Jose Otero, Visión global sobre seguridad en redes,servicios y aplicaciones.

## Tablas de contenido

* **aux:** directorio con todo el soporte visual del trabajo.
* **contenedores:** script para descargar los dockers utilizados en este trabajo.
* **introduccion-seguridad.gif:** Pequeño gif, donde se muestran los utiles con los que se trabajará.
* **introduccion-seguridad.mp4:** Pequeño mp4, donde se muestran los utiles con los que se trabajará.
* **presentacion.html:** Presentación en html para la defensa ante el tribunal.
* **poster.pdf:** Pdf en formato *campalans* sobre el contenido del trabajo.

## Descripción del proyecto

Este trabajo pivota sobre la idea de seguridad, se detallan tanto los
mecanismos básicos de pentesting incluidos en la distribución de kali-linux,
como las herramientas de detección de intrusos que nos ofrece el mercado
opensource, también se discute sobre las estrategias básicas para obtener cierto anonimato en la red.

