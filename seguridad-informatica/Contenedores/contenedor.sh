#!/bin/bash


#descargar imagenes 

docker pull joterop/kali
docker pull hmlio/vaas-cve-2014-0160
docker pull tuxotron/audi_sqli
docker pull hmlio/vaas-cve-2014-6271

# Lanzar dockers 

docker run -it joterop/kali /bin/bash
docker run -d -p 8443:443 --name Contenedor_HeartB hmlio/vaas-cve-2014-0160
docker run -d -p 80:80 --name Contenedor_SQLi tuxotron/audi_sqli
docker run --name bashdoor -d hmlio/vaas-cve-2014-6271

