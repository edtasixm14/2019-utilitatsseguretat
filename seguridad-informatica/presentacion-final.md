---
theme: metropolis
title: Seguridad informática
author: Jose Otero Perez
header-includes:
    - \usepackage[utf8]{inputenc}
    - \usepackage[catalan]{babel}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \rhead{Escola del Treball}
    - \lhead{Asignatura M09}
    - \rfoot{\thepage}
    - \fancyfoot[CO,CE]{2019}
toc:
    true
---


### Inicio

![](aux/util.png)

### Desarrollo

![](aux/pentesting.png)

### Mostrar peligros internet VPS

* Debemos concienciarnos de los peligros de internet

* ![](aux/captura.png)

* 2000 intentos de acceso en la ultima semana

* 338 ips distintas

* 61 países distintos


### Discernir sobre Pentesting, NIDS, privacidad

* Pentesting o Penetration Testing es la práctica de atacar diversos entornos con la intención de descubrir fallos, vulnerabilidades u otros fallos de seguridad, para así poder prevenir ataques externos hacia esos equipos o sistemas. Es una rama de estudio relativamente reciente y en auge (sobrevenido por los importantes ataques y filtraciones sufridos por varias empresas importantes los últimos años).

* Un sistema de detección de intrusiones (o IDS de sus siglas en inglés Intrusion Detection System) es un programa de detección de accesos no autorizados a un computador o a una red.

* Aquello que hemos perdido cuando navegamos por internet.


### Kali linux

* Kali linux es una distribución linux basada en Debian, que tiene como finalidad última ofrecer un conjunto de herramientas informáticas capaces de realizar auditorias de seguridad.

* ![](aux/kali.png)


### NMAP

Filtrado de puertos y de vulnerabilidades gracias al NSE

`nmap -sV --script (nombre del script) (ip destino)`

```
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-11 20:18 UTC
Nmap scan report for 172.17.0.3 (172.17.0.3)
Host is up (0.000018s latency).
Not shown: 999 closed ports
PORT    STATE SERVICE  VERSION
443/tcp open  ssl/http Apache httpd 2.2.22 ((Debian))
|_http-server-header: Apache/2.2.22 (Debian)
| ssl-heartbleed:
|   VULNERABLE:
|   The Heartbleed Bug is a serious vulnerability in the popular OpenSSL cryptographic software library. It allows for stealing information intended to be protected by SSL/TLS encryption.
|     State: VULNERABLE
|     Risk factor: High
|       OpenSSL versions 1.0.1 and 1.0.2-beta releases (including 1.0.1f and 1.0.2-beta1) of OpenSSL are affected by the Heartbleed bug. The bug allows for reading memory of systems protected by the vulnerable OpenSSL versions and could allow for disclosure of otherwise encrypted confidential information as well as the encryption keys themselves.
|           
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0160
|       http://cvedetails.com/cve/2014-0160/
|_      http://www.openssl.org/news/secadv_20140407.txt
MAC Address: 02:42:AC:11:00:03 (Unknown)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 13.65 seconds


```
### Metasploit

* `Metasploit` es una software libre que tiene una base de datos que nos permite acceder a maquinas remotas explotando vulnerabilidades y ejecutando payloads a través  de estas.

* Se basa en una base de datos que contiene `exploits` y otra que contiene `payloads` llamada meterpreter.

* Un `payload` es una pequeña aplicación que aprovecha una vulnerabilidad para obtener el control del sistema remoto.


### Sqlmap

Sqlmap es una aplicación desarrollada en Python que tiene como objetivo realizar ataques Sql injection de manera automatizada

* Vulnerabilidades que permite enviar instrucciones SQL de manera mal intencionada.

* select * from usuarios where usuario = 'jose' or '1' = '1';

### Hydra

Hydra es una aplicación  que tiene como objetivo crackear `logins`, tiene compatibilidad con muchos protocolos HTTP, FTP, TELNET, IMAP, SMB, SSH...

```
Existen principalmente dos modos de uso, usando diccionarios, y usando la fuerza bruta:

root@3bdc61718de1:/# hydra -L users.txt -P passlist.txt ftp://172.17.0.2

con la opción -L le pasamos una lista de usuarios y con la opción -P una lista de passwords, para que itere las combinaciones.

```

```

-x 1:3:[] el primer y segundo número indican la longitud mínima y máxima de la combinación  de caracteres creada, y la tercera indica el tipo de caracteres, podemos usar "/", "1", "a", "A" para indicar números, mayúsculas o minúsculas, incluso slashes.


```

### Jhon the Ripper

`Jhone de Ripper` es una utilidad que aglutina un conjunto de algoritmos de crackeo, capaz de hacer frente, con mayor o con menor éxito los siguientes formatos, traditional DES-based, "bigcrypt", BSDI extended DES-based, FreeBSD MD5-based  and OpenBSD Blowfish-based, Kerberos/AFS and Windows LM (DES-based) hashes, DES-based tripcodes, SHA-crypt hashes (Fedora y Ubuntu)

* Single crack mode, información derivada de Gecos
* Diccionario
* Incremental
* Modular

___

### IDS

* Detección de intrusos

* Network Intrusion Detection System

* Host-Based Intrusion Detection System

### SNORT

* Defensa activa

* Sniff de red

* Creación de reglas por parte de la comunidad


### TRIPWIRE

* Defensa pasiva

* Creación de base de datos encriptada

* comprobación periódica de la integridad de los ficheros monitorizados.


___

### privacidad

* Tenemos privacidad?

* Somos conscientes de ello?

* Es nuestra información personal el negocio del siglo XXI




### PROXY

* Enmascarar comunicaciones

* Filtrar y cachear conexiones web

* proteger servidores web con proxys inversos.

* Problemas de seguridad SSL bump

### Proxy desde la web al servidor

![](aux/proxy1.png)

### Desde el proxy hasta la web remota

![](aux/proxy2.png)

### VPN

* Conexión cifrada punto a punto

* Problemas con los modelos Freemium

* Problemas de transparencia por parte de los proveedores.

### VPN servicios

* Evitar empresas ubicadas en:

```
USA, UK, Canadá, Nueva Zelanda, Australia, Dinamarca, Francia, Holanda, Noruega, Alemania, Bélgica, Italia, Suecia, España, Israel, Japón, Corea del Sur.

```

* Buscar opciones con killswitch

* Precio entre 3 y 7 euros por mes

### VPN problemas históricos

* HolaVPN

* Cuidado con lo que firmas

* BOTnet

* 8chan


### TOR

* Algoritmo de cifrado muy robusto

* Problemas de concepto

* Tipos de búsquedas

* tor-relay y tor-exit

* Mala idea usar protocolos http

### NO usar http + tor

* Uso `httpry` para snifar el tráfico del nodo de salida

* "GET http://api.steampowered.com/ISteamUser/GetFriendList/v1/?key=97B191ED0FFD88DB0BE9613DF53E400D&steamid=76561198869819982 HTTP/1.1"


* ![](aux/steam.png)

### Análisis del tráfico

![](aux/grafico-tor.png)

### Ejemplos prácticos

### Metasploit Heartbleed

![](aux/heartbleed.gif)

### Metasploit shellshock

![](aux/shellshock.gif)

### Jhon the ripper

![](aux/john.gif)

### Hydra

![](aux/hydra.gif)

### Sqlmap + Snort + Bot telegram

![](aux/snort+telegram.gif)

### Bug Vim

![](aux/shell-reverse.gif)

:!uname -a||" vi:fen:fdm=expr:fde=assert_fails("source\!\ \%"):fdl=0:fdt="
